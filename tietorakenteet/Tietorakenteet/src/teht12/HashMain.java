package teht12;

import java.util.Scanner;

/**
 *
 * @author Meri Alho
 */
public class HashMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int valinta=0, arvo;
        HashTable hash = new HashTable(); 
        Scanner s = new Scanner(System.in);       
        
        while (valinta!=4){
            System.out.println();
            System.out.println("\t\t\t1. Generoi arvot");
            System.out.println("\t\t\t2. Etsi arvo");
            System.out.println("\t\t\t3. Tulosta arvot");
            System.out.println("\t\t\t4. Lopetus\n");
            valinta = s.nextInt();
            
            if (valinta==1){
                    
                for (int i=0; i<11; i++){
                    arvo = (int) (Math.random()*600) +1;
                    hash.addKey(arvo);
                    System.out.println(i + ": " + arvo);
                    
                }
                System.out.println();
                hash.print();
            }
            if (valinta==2){
                System.out.println("Syötä etsittävä arvo:");
                arvo = s.nextInt();
                hash.searchKey(arvo);
                //System.out.println("Arvo " + arvo + " löytyi indeksistä: " + index);

            }
            if (valinta==3){
                hash.print();
            }
            if (valinta==4){
                break;
            }
        }

    }
    
}
