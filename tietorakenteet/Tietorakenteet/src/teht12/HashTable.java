package teht12;

/**
 *
 * @author Meri Alho
 */
public class HashTable {
    
    private int size = 23;
    private int[] array;
    private int full;
    
    public HashTable(){
        array = new int[size];
        this.size = size;
        full = 0;
    }
    
    public void addKey(int key){
        int index = key%size;
        
        if (key == 0){
            // ei sallita nollaa
        }
        if (array[index] == 0){
            array[index] = key;
            full++;
        } else {
            collision(index, key);
        }
        
    }
    
    public void searchKey(int key){
        int index = key%size;
        boolean found = false;
        
        if (array[index]!= key){
            
            while(!found){
                index++;
                if (index>=size){
                System.out.println("Arvoa ei löytynyt!");
                break;
                }
                if (array[index] == key){
                    found=true;
                } else {
                    found=false;
                }
                
            }
            if (index==size){
            } else{
            System.out.println("Arvo löytyi indeksillä: " + index);
            }
        } else {
            found=true;
            System.out.println("Arvo löytyi indeksillä: " + index);
        }
        
    }
    
    public void collision(int index, int data){
        boolean onnistui = false;
        int paikka; 
        
        paikka=index+1; //uusi paikka
        
        while(!onnistui && full!=size){
            if (paikka>=size){
                paikka=0;
                }
            if (array[paikka]==0){
                array[paikka] = data; //talleta avain
                full++;
                onnistui = true;
            } else {
                paikka=paikka+1; //uusi paikka
               
            }
        }
    }
    
    public void print(){
        System.out.print("Taulukko: ");
        for (int i=0; i<size; i++){
            System.out.print(array[i] + ", ");
        }
        System.out.println();
    }
}
