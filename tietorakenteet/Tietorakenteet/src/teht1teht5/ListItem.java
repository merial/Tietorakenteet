/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht1teht5;

/**
 *
 * @author Meri Alho
 */
public class ListItem {
    private String mData;
    private ListItem next;

    public ListItem(){
        next = null;
    }
    
    public String getData() {
        return mData;
    }

    public void setData(String mData) {
        this.mData = mData;
    }

    public ListItem getNext() {
        return next;
    }

    public void setNext(ListItem next) {
        this.next = next;
    }
    
    
}
