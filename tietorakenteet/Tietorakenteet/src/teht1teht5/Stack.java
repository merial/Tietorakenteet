/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht1teht5;

import java.util.*;
/**
 *
 * @author Meri Alho
 */
public class Stack {

    private LinkedList<String> list;
    private ListItem top;
    private int size;
    
    public Stack(){
        list = new LinkedList<String>();
        //top = null;
        //size = 0;
    }
    
    //muodosta uusi lista-alkio ja aseta se pinon huipulle
    public void push(String aData){
        list.push(aData);
        //ListItem newItem = new ListItem(); 
        //newItem.setData(aData);
        //newItem.setNext(top); //kytketään aikaisempaan huippualkioon
        //top = newItem; //uusi alkio huippualkioksi
        //size++;
        
    }
    
    //palautetaan alkio pinon huipulta, jos pino on tyhjä, palautetaan null
    public String pop(){
        
        String eka = list.getFirst();
        list.pop();
        
        //ListItem apu = new ListItem();
        //apu = top;
        //top = top.getNext();
        //size--;
        //return apu;
        
        return eka;
    }
    
    //tulosta pinon alkiot
    public void printItems(){
        /*ListItem item = new ListItem();
        item = top;
        System.out.println(item.getData());
        int apuSize = size-1;        
        for(int i = size; i>=0; i--){
            if (item.getNext()!=null){
               item = item.getNext();
               System.out.println(item.getData());                
            }
        }**/
        ListIterator<String> iter = list.listIterator();
        
        while(iter.hasNext()){
            System.out.println(iter.next());
        }
        
    }
    
    //palautetaan pinottujen alkioiden lukumäärä
    public int getSize(){
        return list.size();
    }
    
}
