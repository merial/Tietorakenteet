package teht11;

/**
 *
 * @author Meri Alho
 */
public class BinaryHeapMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BinaryHeap heap = new BinaryHeap(15);
        int value;
        
        for (int i=0; i<15; i++){
            value = (int) (Math.random()*100);
            heap.insert(value);
        }
        heap.print();
        heap.removeMin();
        heap.print();
        heap.removeMin();
        heap.print();
        
        
    }
    
}
