package teht10;

import java.util.Scanner;

/**
 *
 * @author Meri Alho
 */
public class BinarySearchMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        BinarySearch search = new BinarySearch(30);
        Scanner s = new Scanner(System.in);
        int valinta=0;
        
        
        
        search.print();
        
        
        while (valinta!=4){
            System.out.println();
            System.out.println("\t\t\t1. Generoi arvot");
            System.out.println("\t\t\t2. Etsi arvo");
            System.out.println("\t\t\t3. Tulosta arvot");
            System.out.println("\t\t\t4. Lopetus\n");
            valinta = s.nextInt();
            
            if (valinta==1){
                    
                for (int i=0; i<30; i++){
                    int arvo = (int) (Math.random()*600);
                    search.keyAdd(arvo);
                    //System.out.println("loop " + arvo);
                    
                }
                
                System.out.println();
                search.print();
            }
            if (valinta==2){
                System.out.println("Syötä etsittävä arvo:");
                int arvo = s.nextInt();
                int indeksi = search.keySearch(arvo);
                
                if (indeksi==666){
                    System.out.println("Haettua avainta ei löytynyt");
                }
                else {
                    System.out.println("Avain löytyi indeksillä: " + indeksi);
                }

            }
            if (valinta==3){
                search.print();
            }
            if (valinta==4){
                break;
            }
        }
    }
    
}
