package teht10;

/**
 *
 * @author Meri Alho
 */
public class BinarySearch {
    private int[] array;
    private int size;
    private int apu;
    private int oldKey;
    private int middle;

    public BinarySearch(int arraySize){
        size = 0;
        array = new int[arraySize];
    }
    
    public void keyAdd(int keyValue){
        
        int lower=0, upper=size-1, j=0, cur=0;
        
        if (size==0){
                array[0]=keyValue;                
        } else {
            while (lower <= upper){
                cur = (lower + upper ) / 2;

                if (array[cur] == keyValue){
                    break;
                } else if(array[cur] < keyValue){
                    lower = cur + 1;
                } else if(array[cur] > keyValue){
                    upper = cur - 1;
                }
            }
        }
        
        if(array[cur] < keyValue){       
            j = cur + 1;
        } else {
            j = cur;
        }
        for(int k = size; k > j; k--){
            array[k] = array[k-1];
        }
        array[j] = keyValue;
        size++;
        
    }
        
    public int keySearch(int key){
        int index=666;
        if(size%2 == 0){ //parillinen määrä alkioita
            middle = size/2;
        } else { //pariton määrä alkioita
            middle = (size+1)/2; 
        }
           
        if(array[middle] > key){ 
        //jos key on pienempi kuin arrayn keskimmäinen arvo
            
            for (int i=middle; i>=0; i--){ 
            //käydään array keskeltä taaksepäin
                if (array[i]==key){
                    index=i;
                    break;
                }
            }
        } else if (array[middle] == key){ 
        //key löytyi heti keskeltä
            index=middle;
        } else { //jos key on isompi kuin arrayn keskimmäinen arvo
            for (int i=middle; i<=size; i++){ 
            //käydään array läpi keskeltä eteenpäin
                if (array[i]==key){
                    index=i;
                    break;
                }
            }
        }
    //palautetaan indeksi, jossa haettu arvo sijaitsee  
    return index;
    }
    
    public void print(){
        for (int i=0; i<size; i++){
            System.out.println(i+1 + ". arvo: " + array[i]);
        }
    }
   
}
