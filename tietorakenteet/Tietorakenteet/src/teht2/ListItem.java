/*
 * 
 */
package teht2;

/**
 *
 * @author Meri Alho
 */
public class ListItem {
    private String mData;
    private ListItem next;

    public ListItem(){
        next = null;
    }
    
    public String getData() {
        return mData;
    }

    public void setData(String mData) {
        this.mData = mData;
    }

    public ListItem getNext() {
        return next;
    }

    public void setNext(ListItem next) {
        this.next = next;
    }
    
    
}
