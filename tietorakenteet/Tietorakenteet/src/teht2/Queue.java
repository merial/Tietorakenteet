/*
 * 
 */
package teht2;

/**
 *
 * @author Meri Alho
 */
public class Queue {

    private ListItem top;
    private ListItem last;
    private ListItem prev;
    private int size;
    
    public Queue(){
        top = null;
        size = 0;
        last = null;
    }
    
    //muodosta uusi lista-alkio ja aseta se jonon viimeiseksi
    public void push(String aData){
        ListItem newItem = new ListItem(); 
        newItem.setData(aData);
        if (last!=null) last.setNext(newItem); //kytketään aikaisempaan viimeiseen alkioon
        if(top==null){
            top = newItem;
        }
        last = newItem; //uusi alkio viimeiseksi alkioksi
        
        size++;
        
    }
    
    //palautetaan alkio pinon huipulta, jos pino on tyhjä, palautetaan null
    public ListItem pop(){
        ListItem apu = top;
        top = top.getNext();
        size--;
        return apu;
    }
    
    //tulosta alkiot
    public void printItems(){
        ListItem item = top;
        System.out.println(item.getData());
        
        for(int i = 0; i<= size; i++){
            if (item.getNext()!=null){
               item = item.getNext();
               System.out.println(item.getData());
                
            }
            
        }
    }
    
    //palautetaan alkioiden lukumäärä
    public int getSize(){
        return size;
    }
    
}
