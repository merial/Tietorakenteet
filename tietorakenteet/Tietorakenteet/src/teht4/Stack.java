/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package teht4;

/**
 *
 * @author kamaj
 */

public class Stack {

        ListItem top; // top näkyy oletuspakkaukseen
        //String top;
        int size;
        ListItem[] lista;

        public Stack() {
            lista = new ListItem[10];
            top = null;
            size = 0;
        }
        //  palautetaan pino-iteraattori
        public StackIterator iterator() {
            return new StackIterator(this);
        }
        // muodostetaan uusi alkio ja viedään se huipulle
        public void push(String aData) {
                ListItem newItem = new ListItem(); // luodaan uusi lista-alkio
                newItem.setData(aData);
                //newItem.setLink(top); // kytketään uusi alkio aikaisempaan huippualkioon
                //top = newItem; // uusi alkio pinon 1:ksi
                lista[size] = newItem;
                size++;
                top = lista[size-1];
        }
        
        // poistetaan alkio pinon huipulta, jos pinossa ei alkioita palautetaan null
        public String pop() {
            //ListItem takeAway;
            ListItem takeAway;
            
                if (top == null) {
                        takeAway = null; // pino on tyhjä
                }
                else{
                        takeAway = lista[size-1];
                        lista[size-1] = null;
                        size--;
                        top = lista[size-1];
                }
                return takeAway.getData();
        }
        
        // palautetaan pinottujen alkioiden lukumäärä
        public int amount() {
                return size;
        }
        // listataan sisältö
        public void printItems() {
            ListItem lPointer = top;
            int indeksi=size-1;
            
                while (indeksi >= 0) {                       
                    if (lPointer!=null){
                        lPointer = lista[indeksi];
                        System.out.print(lPointer.getData()+", ");
                        indeksi--;
                        
                    }
                }
        }
}

