/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package teht4;

/**
 *
 * @author kamaj
 */
interface Iterator {
    boolean hasNext();
    Object next();
}

public class StackIterator implements Iterator {
    private ListItem current;
    private Stack container; // container on tietorakenne, jota iteroidaan
    private int aTop;

    StackIterator (Stack c) { // konstruktori on "package visible"
        container = c;
        current = container.lista[container.size-1];
        aTop = container.size;
    }
    // palautetaan tieto siitä, löytyyko rakenteesta seuraava alkio
    // hmm... palautetaan tieto siitä, osoittaako nykypositio (current) alkiota vai ei.
    public boolean hasNext() {
        if (current == null)
            return false;
        else
            return true;
    }
    // palautetaan nykyinen (lista-alkio) ja siirretään nykypositiota pykälä eteenpäin
    public String next() {
        ListItem oldCurrent = current;
        if(aTop-1<0){
        oldCurrent = null;
        }else{
            oldCurrent = current;
            aTop--;
            if(aTop-1<0){
                current = null;
                aTop=container.size;
            } else {
                current = container.lista[aTop-1];
            }
        }
        return oldCurrent.getData(); 
    }

}
