/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package btree;

/**
 *
 * @author kamaj
 */
public class BinaryTree {

    private Node root;
    int value;
    
    public BinaryTree(String rootValue) {
        root = new Node(rootValue);
    }
    public BinaryTree(){
        root = null;
    }
    /*public BinaryTree(String rootValue, BinaryTree left, BinaryTree right){
        root = new Node(rootValue, left, right);
    } */
    
    public void insert(String aData){
        Node newnode = new Node(aData);
        
        /*if (aData.compareTo(root.getData())== 0){
            System.out.println("Duplikaatti");
            return;
        }*/
        if (root == null){            
            root = newnode;
        }
        //Node current = root;
        //Node parent = null;
        String rootdata = root.getData();
        int str = rootdata.compareTo(aData);
        //parent = current;
        
        if (str < 0 ){
                if (root.left()==null){
                    root.setLeft(new BinaryTree(aData));
                }else{ 
                    root.left().insert(aData);
                }
        }
        else if (str > 0 ){
            if(root.right()==null){
                root.setRight(new BinaryTree(aData));
            }
            else{ 
                root.right().insert(aData);
                
            }
        }
    }
    
    public BinaryTree find(String aData){
        Node current = root;
        String rootdata = root.getData();
        int str = rootdata.compareTo(aData);
        
        while(current!=null){
            
            if (str == 0){
                return new BinaryTree(aData);
            }
            if (str < 0 ){
                if (root.left()==null){
                    return current.left();
                } else { 
                    return current.left().find(aData);
                }
            }
            else if (str > 0 ){
                if(root.right()==null){
                    return current.right();
                } else { 
                    return current.right().find(aData);
                }
            }
        }
        return null;
    }
    
    public boolean remove(String aData){
        Node parent = root;
        Node current = root;
        boolean isLeftChild = false;
        String rootdata = current.getData();
        int str = rootdata.compareTo(aData);
        
            if (str < 0 ){
                if (root.left()==null){
                    isLeftChild = true;
                    return false;
                } else { 
                    return current.left().remove(aData);
                }
            }
            else if (str > 0 ){
                if(root.right()==null){
                    return false;
                } else { 
                    return current.right().remove(aData);
                }
            } else {
                if (current.left()!= null && current.right()!= null){
                    this.value = current.right().minValue();
                    current.right().remove(aData);
                } else if (parent.left() == this) {
                    
                } else if (parent.left() == this) {
                    
                }
                return true;
            }
    }
    
    public int minValue(){
        
        if (root.left()==null){
            return value;
        }
        else {
            return root.left().minValue();
        }
    }
    
    public void preOrder() {
        if (root != null) {
            System.out.println(root.getData()+',');
            if (root.left() != null) // pääseeekö vasemmalle?
                root.left().preOrder();
            if (root.right() != null) // pääseekö oikealle?
                root.right().preOrder();
        }

    }
  
    public void setLeft(BinaryTree tree) {
        root.setLeft(tree);
    }

    public void setRight(BinaryTree tree) {
        root.setRight(tree);
    }
}
