package laskin;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

// Luokan nimen loppu pitää olla Test
public class LaskinTest {

    // Fixture ("vakiokaluste"): kaikki testit käyttävät samaa laskinta,
    // joka nollataan ennen kutakin testiä.
    private static Laskin laskin = new Laskin();

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void clearCalculator() {
        laskin.nollaa();
    }

    @After
    public void tearDown() throws Exception {
    }

    // Testimetodin nimi voi olla mitä tahansa, edessä annotaatio @Test    
    @Test
    public void testLisaa() {
        laskin.lisaa(1);
        laskin.lisaa(1);
        assertEquals("1 + 1 ", 2, laskin.annaTulos());
    }

    @Test
    public void testVahenna() {
        laskin.lisaa(10);
        laskin.vahenna(2);
        assertEquals("10 - 2 ", laskin.annaTulos(), 8);
    }

    @Test
    public void testJaa() {
        laskin.lisaa(8);
        laskin.jaa(2);
        assert laskin.annaTulos() == 5;
    }

    // Nollalla jaon pitäisi heittää poikkeus
    @Test(expected = ArithmeticException.class)
    public void testJaaNollalla() {
        laskin.jaa(0);
    }

    // Tätä testiä ei haluta vielä ajaa
    @Test
    @Ignore("Metodi multiply() on vielä toteuttamatta.")
    public void testKerro() {
        laskin.lisaa(10);
        laskin.kerro(10);
        assertEquals("10 * 10 ", 100, laskin.annaTulos());
    }

    /**
     * Test of nollaa method, of class Laskin.
     */
    @Test
    public void testNollaa() {
        System.out.println("nollaa");
        Laskin instance = new Laskin();
        instance.nollaa();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of annaTulos method, of class Laskin.
     */
    @Test
    public void testAnnaTulos() {
        System.out.println("annaTulos");
        Laskin instance = new Laskin();
        int expResult = 0;
        int result = instance.annaTulos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of nelio method, of class Laskin.
     */
    @Test
    public void testNelio() {
        System.out.println("nelio");
        int n = 0;
        Laskin instance = new Laskin();
        instance.nelio(n);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of neliojuuri method, of class Laskin.
     */
    @Test
    public void testNeliojuuri() {
        System.out.println("neliojuuri");
        int n = 0;
        Laskin instance = new Laskin();
        instance.neliojuuri(n);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of virtaON method, of class Laskin.
     */
    @Test
    public void testVirtaON() {
        System.out.println("virtaON");
        Laskin instance = new Laskin();
        instance.virtaON();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of virtaOFF method, of class Laskin.
     */
    @Test
    public void testVirtaOFF() {
        System.out.println("virtaOFF");
        Laskin instance = new Laskin();
        instance.virtaOFF();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
